package oculus.tablist.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class NumberUtil {
    private static final DecimalFormat twoDPlaces = new DecimalFormat("#,###.##");

    static {
        twoDPlaces.setRoundingMode(RoundingMode.HALF_UP);
    }

    public static String formatDouble(final double value) {
        return twoDPlaces.format(value);
    }
}
