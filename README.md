# About Tablist

Tablist is a simple Tablist plugin for Minecraft. \
It Supports [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/). \
You can configure all in the **plugins/Tablist/[config.yml](https://gitlab.com/Commandcracker/tablist/-/blob/master/src/main/resources/config.yml)** file.

## Requirements

Tablist currently supports **CraftBukkit**, **Spigot** and [Paper](papermc.io/) **(recommended)**. \
Other server implementations may work, but we don't recommend them as they may cause compatibility issues. \
[PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/) (Optional)

## Links

|Source|Artifacts|Stats|
|:---:|:---:|:---:|
|[Gitlab](https://gitlab.com/Commandcracker/tablist)|[Spigot](https://www.spigotmc.org/resources/tablist.89241)|[bStats (Bukkit / Spigot)](https://bstats.org/plugin/bukkit/Tab-List/9987)|
| |[Bukkit](https://dev.bukkit.org/projects/commandcrackers-tablist)|[bStats (Bungeecord)](https://bstats.org/plugin/bungeecord/Tab-List/11544)|
| |[CurseForge](https://www.curseforge.com/minecraft/bukkit-plugins/commandcrackers-tablist)|

## Metrics collection

Tablist collects anonymous server statistics through bStats, an open-source Minecraft statistics service.

### Bukkit / Spigot

[![bStats](https://bstats.org/signatures/bukkit/Tab-List.svg)](https://bstats.org/plugin/bukkit/Tab-List/9987)

### Bungeecord

[![bStats](https://bstats.org/signatures/bungeecord/Tab-List.svg)](https://bstats.org/plugin/bungeecord/Tab-List/11544) \
If you'd like to disable metrics collection via bStats, you can edit the **plugins/bStats/config.yml** file.
